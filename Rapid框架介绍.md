# 概述
Rapid(激流)是一款集日志服务、依赖注入、OSGi服务、UI组件、同步/异步事件系统、同步/异步动作系统(支持撤销)、微组件系统(支持热安装和热卸载)、
数据持久化、支持多进程的移动端应用快速开发框架<br>
Rapid是OSGi标准的实现，在微组件开发过程中只需要了解Service和Event即可。

## Service
Service为功能服务提供器，所有功能全部由服务提供，相当于SpringBoot中的Service，
全局都可以取任意的服务调用其功能，实现功能复用。与此同时，在插件中，Service还支持依赖注入，同时支持构造注入和字段注入。
比如权限功能由PermissionService提供，全局都可以通过Framework获取该Provider：
```java
void requestPermissions() {
    var service = Platform.getService(CommonServiceKeys.PERMISSION);
    service.requestPermission(xxx);
}
```
或者在插件中使用依赖注入：
```java
class MyExtension {
    // 在构造方法注入
    Student student;
    // 使用字段注入
    @Import
    CommandService commandService;
    // 构造注入 Student Data Bean
    MyExtension(Student student) {
        this.student = student;
    }
    
    private void init() {
        commandService.registerCommand(...)
    }
}
```

## Event
Event为事件，某些动作或Service的功能会存在事件触发，事件在全局都可以被调用，例如：
```java
Platform.getFramework().dispatchEvent(CommonEventKeys.CHECK_UPDATE, event -> {
    event.onNoUpdate();
});
```
Event可以注册多个，只需要用相同的Key注册就行了，例如：
```java
Platform.getFramework().addEvent(PorjectEventKeys.LIFECYCLE, ...);
```

## Data
Data为数据，数据不一定是通常意义上的数据，也有可能是Activity/Fragment等， View/Service/Event的交互少不了数据的穿插，
在数据传递过程中，保证数据只被全局的DataHolder所持有，DataHolder生命周期与软件生命周期一致