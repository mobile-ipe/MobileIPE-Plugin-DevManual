# QiStudio 架构
QiStudio 采用Java-Core NT架构，基于Java搭建了能够在桌面端和安卓端跨平台运行的服务核心，在各平台的QiStudio 只需要实现UI即可

## QiStudio 核心
QiStudio 参考了OSGi规范，在QiStudio 内部实现了一个巨大的OSGi容器，将服务端SOA思想带到了安卓开发中。
按照OSGi规范，QiStudio 的核心组件是 OSGi 框架。这个框架为App和微组件（QiStudio 的扩展程序）提供了一个标准环境。<br>
QiStudio的核心框架实现为[Rapid框架](Rapid框架介绍.md)

## OSGI框架简介
OSGI框架从概念上可以分为三层：模块层、生命周期层和服务层。
1. Module Layer：模块层主要涉及包及共享的代码；
2. Lifecycle Layer：生命周期层主要涉及组件的运行时生命周期管理；
3. Service Layer：服务层主要涉及模块之间的交互和通信。

![img.png](images/osgi-framework.png)
<br>
基于OSGI的这些基础概念，QiStudio 做了如下实现：<br>
1. OSGI Framework是OSGI Service Platform规范的核心组成部分，提供了一个通用的、安全可管理的Java framework。
通过OSGI Framework可以支持Service application的部署和扩展。
在QiStudio 中QiStudio 提供了这样的Framework(QiCore)来实现组件的部署和扩展。

2. OSGI兼容设备可以下载并且安装OSGI组件，也可一旦不再需要的时候删除。
组件安装后会注册一定数量的Services(在QiStudio 中也称为Services)，
并被由同一个Framework(在QiStudio 中只存在Global Framework和Project Framework)下的其它组件使用。
在一个动态扩展的的OSGI环境中，Framework管理组件的安装和更新，同时也管理组件和Services之间的依赖关系。

3. 其次，Framework为Java组件开发者提供了简明一致的编程模型，简化了开发部署的复杂性。
编程模型允许开发者将自己的接口规范绑定到OSGI环境中的Service。

### 模块层
模块层是OSGi框架中最基础的部分。
OSGi的模块化，是通过为平台程序包添加配置文件(program.json)来定义其控制单元(在OSGI中称为Bundle)<
Bundle是以平台程序包形式存在的一个模块化物理单元，包含代码、资源文件和配置文件，
Bundle是OSGi中的基本组件，其表现形式仍然为Java概念中传统的Jar包(在QiStudio 安卓平台上为Apk、Lua脚本、RPC服务件等微组件格式)。

### 生命周期层
生命周期层在OSGi框架中属于模块层上面的一层，生命周期层的运作是建立在模块层的功能之上的。
生命周期层的主要功能是控制动态安装、开启、关闭、更新和卸载组件。
生命周期层能够从外部管理应用或者建立能够自我管理的应用（或者两者的结合），并且给了应用本身很大的动态性。
Bundle的使用需要生命周期层的API与OSGi框架的生命周期层进行交互。
但OSGi框架的核心并没有强制使用任何特定的API交互机制（比如命令行，GUI，或者XML配置文件等），只是单纯的Java API而已，
开发者可以任意创造出自己想要的交互模式，保证了框架的灵活性。因此，QiStudio 在框架中引入了事件(Event)系统和动作(Action)系统，
来简化应用间的交互。

### 服务层
OSGi服务是注册到OSGi框架中的一个Java对象。注册的时候可以设置Service(QiStudio 中为Service、Event、Action等组件)的属性。
Bundle可以通过QiStudio 的IOC容器去注册Service或去查询Service。

![img.png](images/osgi-service.png)

### OSGI模块化与面向对象
面向对象编程中不会把所有功能都塞到同一个类。面向对象编程从问题域中发现多个事物，每个事物负责不同的功能，
尽量做到高内聚和低耦合。面向对象的模块化粒度在类级别上。
QiStudio 的模块化是通过为QiStudio 微组件添加配置文件来定义哪些类应该作为组件加载到QiStudio OSGi容器，其控制可见性的粒度是在组件层面上的。
OSGI模块化和面向对象思想所带来的能力都是通过控制可见性和可用性来保证高内聚和低耦合的，但粒度不同，一个是组件层面上，一个是对象层面上。

### QiStudio 的IOC容器
在OSGi的标准中并没有规定依赖注入(Dependency Injection,简称DI)的实现，QiStudio 为DI的实现提供了IOC容器，
通过IOC容器，能够将微组件开发者所使用的QiStudio 服务组件的赋予权交给QiStudio ，
在开发者的微组件中可以使用依赖注入的方式来获取QiStudio 中已注册的服务组件，
并且通过观察者模式提供了依赖重注入的监听接口将监听服务组件变更的权限交给了微组件开发者