# 扩展点介绍

## platform.dataBean
扩展点名称: platform.dataBean<br>
扩展点描述: 扩展依赖注入的对象<br>
扩展点配置:<br>
 - class: 指定对象的完整类名 
 - name: 指定对象的限定使用名称 
 - attributes: 指定需要初始化的属性，该属性值需为 JSON 对象，以键值对形式指定属性名和属性值

## platform.service
扩展点名称: platform.service<br>
扩展点描述: 扩展依赖注入的服务<br>
扩展点配置:<br>
- class: 指定服务接口类的完整类名,需要确保Service接口类中存在类型为Key，名称为KEY的字段
- implClass: 指定服务实现类的完整类名

## platform.command
扩展点名称: platform.command<br>
扩展点描述: 扩展QiStudio的可执行命令<br>
扩展点配置:<br>
 - class: 指定Command实现类的完整类名，该类需实现 Command 接口

## platform.sweetAgent
扩展点名称: platform.sweetAgent<br>
扩展点描述: 扩展QiStudio的外部服务<br>
扩展点配置:<br>
 - class: 指定SweetAgent实现类的完整类名，该类需实现 SweetAgent 接口 
 - name: 指定SweetAgent的名称

## platform.searchEngine
扩展点名称: platform.searchEngine<br>
扩展点描述: 扩展QiStudio的搜索引擎<br>
扩展点配置:<br>
 - class: 指定搜索引擎实现类的完整类名，该类需实现 SearchEngine 接口

## platform.intentLaunchHandler
扩展点名称: platform.intentLaunchHandler<br>
扩展点描述: 扩展QiStudio处理Intent启动的方式<br>
扩展点配置:<br>
 - class: 处理Intent启动方式的实现类完整类名，该类需实现 IntentLaunchHandler 接口

## platform.settingsBuilder
扩展点名称: platform.settingsBuilder<br>
扩展点描述: 扩展QiStudio的设置项<br>
扩展点配置:<br>
 - class: 指定SettingsBuilder实现类的完整类名，该类需实现 SettingsBuilder 接口

## platform.theme.app
扩展点名称: platform.theme.app<br>
扩展点描述: 扩展软件主题<br>
扩展点配置:<br>
 - class: 指定App主题的实现类对应的类名，该类需实现 AppTheme 接口

## platform.icon
扩展点名称: platform.icon<br>
扩展点描述: 扩展图标<br>
扩展点配置:<br>
 - key: 欲替换图标的键名 
 - fileType: 欲替换文件类型图标的文件后缀名 
 - fileTypes: 欲替换文件类型图标的文件后缀名集 
 - path: 图标文件路径，若为assets下路径，则优先使用Android AssetManager api获取图标

## platform.event.logReceiver
扩展点名称: platform.event.logReceiver<br>
扩展点描述: 扩展日志接收器事件<br>
扩展点配置:<br>
 - class: 指定事件的实现类对应的类名，该类需实现 LogReceiverEvent 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## platform.event.systemReceiver
扩展点名称: platform.event.systemReceiver<br>
扩展点描述: 扩展系统广播接收器事件<br>
扩展点配置:<br>
 - class: 指定事件的实现类对应的类名，该类需实现 SystemReceiverEvent 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## platform.hyperLinkInterceptor
扩展点名称: platform.hyperLinkInterceptor<br>
扩展点描述: 扩展QiStudio的超链接拦截器<br>
扩展点配置:<br>
 - class: 指定超链接拦截器的实现类对应的类名，该类需实现 HyperLinkInterceptor 接口

## domain.menu
扩展点名称: domain.menu<br>
扩展点描述: 扩展领域工程编辑界面右上角菜单<br>
扩展点配置:<br>
 - class: 指定菜单的实现类对应的类名，该类需实现 DomainMenuAction 接口

## domain.menu.windowTab
扩展点名称: domain.menu.windowTab<br>
扩展点描述: 扩展领域工程编辑界面多窗口Tab栏菜单<br>
扩展点配置:<br>
 - class: 指定菜单的实现类对应的类名，该类需实现 TabMenuAction 接口

## domain.menu.editor.code
扩展点名称: domain.menu.editor.code<br>
扩展点描述: 扩展编辑器代码操作菜单<br>
扩展点配置:<br>
 - class: 指定菜单的实现类对应的类名，该类需实现 EditorMenuAction 接口

## domain.menu.editor.text
扩展点名称: domain.menu.editor.text<br>
扩展点描述: 扩展编辑器文本操作菜单<br>
扩展点配置:<br>
 - class: 指定菜单的实现类对应的类名，该类需实现 EditorMenuAction 接口

## platform.theme.editor
扩展点名称: platform.theme.editor<br>
扩展点描述: 扩展编辑器主题<br>
扩展点配置:<br>
 - class: 指定编辑器主题的实现类对应的类名，该类需实现 EditorTheme 接口

## domain.toolPageFactory
扩展点名称: domain.toolPageFactory<br>
扩展点描述: 扩展工具页<br>
扩展点配置:<br>
 - class: 指定工具页创建工厂实现类对应的类名，该类需实现 ToolPageFactory 接口

## domain.event.window
扩展点名称: domain.event.window<br>
扩展点描述: 扩展代码窗口事件<br>
扩展点配置:<br>
 - class: 指定事件的实现类对应的类名，该类需实现 WindowEvent 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## domain.windowFactory
扩展点名称: domain.windowFactory<br>
扩展点描述: 扩展文件打开窗口<br>
扩展点配置:<br>
 - class: 指定窗口创建工厂的实现类对应的类名，该类需实现 WindowFactory 接口 
 - channel: 指定Session的Channel

## project.creationOption
扩展点名称: project.creationOption<br>
扩展点描述: 增加新的项目创建方式<br>
扩展点配置:<br>
 - class: 指定项目创建方式的实现类对应的类名，该类需实现 ProjectCreationOption 接口

## project.menu.file
扩展点名称: project.menu.file<br>
扩展点描述: 扩展项目文件菜单<br>
扩展点配置:<br>
 - class: 指定菜单的实现类对应的类名，该类需实现 FileMenuAction 接口

## project.event.file
扩展点名称: project.event.file<br>
扩展点描述: 扩展项目文件事件<br>
扩展点配置:<br>
 - class: 指定事件的实现类对应的类名，该类需实现 ProjectFileEvent 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.event.fileUi
扩展点名称: project.event.fileUi<br>
扩展点描述: 扩展项目文件UI事件<br>
扩展点配置:<br>
 - class: 指定事件的实现类对应的类名，该类需实现 ProjectFileUiEvent 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.event.lifecycle
扩展点名称: project.event.lifecycle<br>
扩展点描述: 扩展项目生命周期事件<br>
扩展点配置:<br>
 - class: 指定事件的实现类对应的类名，该类需实现 ProjectLifecycleEvent 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.manager
扩展点名称: project.manager<br>
扩展点描述: 增加项目管理器<br>
扩展点配置:<br>
 - class: 指定项目管理器的实现类对应的类名，该类需实现 ProjectManager 接口

## project.template
扩展点名称: project.template<br>
扩展点描述: 增加项目模板<br>
扩展点配置:<br>
 - class: 指定项目模板的实现类对应的类名，该类需实现 Template 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.file.creationOption
扩展点名称: project.file.creationOption<br>
扩展点描述: 扩展文件创建方式<br>
扩展点配置:<br>
 - class: 指定文件创建方式的实现类对应的类名，该类需实现 PSObjectCreationOption 接口 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.file.expansionCase
扩展点名称: project.file.expansionCase<br>
扩展点描述: 扩展文件展开方式<br>
扩展点配置:<br>
 - class: 指定文件展开方式的实现类对应的类名，该类需实现 PSObjectExpansionCase 接口 
 - fileTypes: 指定文件类型（后缀名） 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.file.openingOption
扩展点名称: project.file.openingOption<br>
扩展点描述: 扩展文件打开方式<br>
扩展点配置:<br>
 - class: 指定文件打开方式的实现类对应的类名，该类需实现 PSObjectOpeningOption 接口 
 - fileTypes: 指定文件类型（后缀名） 
 - override: 是否覆盖原有的打开方式，而不是注册 
 - scope: 指定该扩展点实现生效的作用域 
     - unit: 指定作用域单元，目前仅支持 global 和 project 
     - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 

## project.file.openWithEditor
扩展点名称: project.file.openWithEditor<br>
扩展点描述: 扩展文件打开方式<br>
扩展点配置:<br>
 - fileTypes: 指定文件类型（后缀名）

## project.language
扩展点名称: project.language<br>
扩展点描述: 扩展编程语言<br>
扩展点配置:<br>
- class: 指定语言创建工厂实现类对应的类名，该类需实现 LanguageFactory 接口
- scope: 指定该扩展点实现生效的作用域
   - unit: 指定作用域单元，目前仅支持 global 和 project
   - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android

## project.language.engine
扩展点名称: project.language.engine<br>
扩展点描述: 扩展编程语言的语言服务功能<br>
扩展点配置:<br>
- class: 指定语言服务实现类对应的类名，该类需实现 LanguageEngine 接口
- language: 指定对应的语言ID
- scope: 指定该扩展点实现生效的作用域
   - unit: 指定作用域单元，目前仅支持 global 和 project
   - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android

## project.language.operator
扩展点名称: project.language.operator<br>
扩展点描述: 快捷符号拦<br>
扩展点配置:<br>
- class: 指定符号提供器实现类对应的类名，该类需实现 OperatorProvider 接口
- id: 指定对应的语言ID
- scope: 指定该扩展点实现生效的作用域
   - unit: 指定作用域单元，目前仅支持 global 和 project
   - identifier/identifiers: 指定作用域单元标识符，比如工程类型标识符 std.android 