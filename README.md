# 概述
“QiStudio”是新一代开放工程平台，其支持以微组件的形式将工程开发环境集成到平台内， QiStudio将服务端SOA思想带到了Android平台，是移动端首款实现OSGi标准的应用。

## 关于本仓库
本仓库致力于提供QiStudio微组件的开发教程，如果您有好的想法或建议可以提交issue或者进行pull request，感谢您的反馈与支持。

## 目录
- [概述](README.md)
- [QiStudio核心体系介绍](QiStudio核心体系介绍.md)
- [Rapid框架介绍](Rapid框架介绍.md)
- [注册你的微组件——安卓插件](plugin/注册你的微组件——插件.md)
- [配置插件清单](plugin/插件清单配置.md)
- [插件扩展点介绍](plugin/扩展点介绍.md)

## QiStudio微组件能做什么？
包括但不限于：
1. 拓展QiStudio的基本功能
2. 在跨设备、跨网络的情况下为QiStudio提供功能支持（DCSM服务件）
3. 提供针对不同工程的插件，如：Android工程插件、Python工程插件
4. 针对编程领域工程构建过程进行优化
5. 实现Html开发的预览功能
6. ...

只要有想法，基本都可以在QiStudio实现。同时，在微组件的加持下，QiStudio必将大放异彩，希望并感谢各位微组件开发者对开发工作的支持和帮助。

## QiStudio微组件有哪些？
1. 平台特定插件（平台特定插件，如Android平台上具有其特定的插件开发接口和模式）
2. 跨平台插件（也称QiStudio拓展包，使用JS/TS进行开发，可以为不同平台的QiStudio扩展功能）
3. DCSM服务件（通过DCSM分布式组件服务模型为QiStudio扩展功能，其支持在跨设备、跨网络的情况下使用，通常是使用Socket进行连接）
4. 更多还在支持中...

## QiStudio Android插件API的引用
1. 在官方网站或者官方群内下载**qistudio-api-版本.jar**，并在本地代码中依赖
2. 通过远程Maven下载(暂未开放)

```groovy
//在本地依赖(示例)
...
dependencies{
    compileOnly files('libs/qiplat-api-1.0.0-alpha1.jar')
}
...
```

## QiStudio Android插件样例
- [QiStudio插件样例仓库](https://gitee.com/mobile-ipe/QiStudio-PluginSamples.git)：由移动IPE联合成员维护的代码仓库，包含一些QiStudio插件的基本样例。
  - [编辑器抖动和烟花动效插件](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/Editor-Activate)
  - [编辑器彩虹括号插件](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/Editor-Rainbow-Brackets)
  - [仿idea网络请求测试工具](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/HttpRequest)
  - [悬浮窗日志插件](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/FloatWindow-Log)
  - [演示如何在代码界面右上角菜单中添加新的菜单](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/Sample-Menu-Domain)
  - [演示如何添加编辑器菜单](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/Sample-Menu-Editor)
  - [Html工程插件](https://gitee.com/mobile-ipe/QiStudio-PluginSamples/tree/master/Html-ProjectManager)
  - ...
- 欢迎各大开发者提供优秀插件样例。

## QiStudio插件开发交流
目前的QiStudio仍处于内测阶段，希望各位开发者测试相关功能是否完善，并向我们反馈您遇到的问题或者提供好的建议，感谢各位开发者的支持与陪伴。

- QQ群组: 951948592
